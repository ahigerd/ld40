using UnityEngine;

public class LevelAwareBehaviour : MonoBehaviour {
	void OnEnable() {
		GameManager.behaviours.Add(this);
	}

	void OnDisable() {
		GameManager.behaviours.Remove(this);
	}

	public virtual void OnGameStart() {
	}

	public virtual void OnLevelStart() {
	}

	public virtual void OnPlayerSpawn() {
	}

	public virtual void OnLifeStart() {
	}

	public virtual void OnPlayerHeal() {
	}

	public virtual void OnPlayerDamage() {
	}

	public virtual void OnPlayerDeath() {
	}

	public virtual void OnGameOver() {
	}
}
