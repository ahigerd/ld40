using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
class EnemySpawner {
	public int minRange = 0;
	public int maxRange = 0;
	public GameObject prefab = null;
	public float weight = 1.0f;
}

[System.Serializable]
class LevelDefinition {
	public Material material = null;
	public int killsPerDrop = 10;
	public int levelLength = 2000;
	public int spawnDensity = 3;
	public List<EnemySpawner> spawners = new List<EnemySpawner>();
}

class TunnelGenerator : LevelAwareBehaviour {
	public static TunnelGenerator instance = null;

	public class Node {
		public Vector3 position;
		public Vector3 forward;
		public Vector3 up;
		public Mesh mesh;

		public Node(Vector3 position, Vector3 forward, Vector3 up) {
			this.position = position;
			this.forward = forward;
			this.up = up;
		}
	}

	public PointText pointTextPrefab = null;
	public AudioClip levelCompleteSound = null;
	public Camera bgCamera = null;
	public Color fogColor = Color.black;
	public Color portalColor = Color.white;
	public AudioClip titleMusic = null;
	public List<AudioClip> levelMusic = new List<AudioClip>();
	public List<LevelDefinition> levels = new List<LevelDefinition>();

	[HideInInspector]
	public int killsPerDrop = 10;
	public List<ShipBehaviour> drops = new List<ShipBehaviour>();
	[HideInInspector]
	public List<EnemySpawner> spawners = new List<EnemySpawner>();
	[HideInInspector]
	public Material material = null;
	public Transform tracker;
	public float speed = 1.0f; // 1 means one segment per second
	public float segmentSize = 5f;
	public float tunnelRadius = 20f;
	[HideInInspector]
	public int totalCount = 2000;
	public int drawDistance = 10;
	private int spawnDensity = 3;
	private int segmentIndex = 0;
	private float segmentPosition = 0.0f;
	private List<Node> rail = new List<Node>();

	private float stepDelta = 0;
	private float stepPhi = 0;
	private int stepsRemaining = 0;

	private Mesh SegmentBetween(Node before, Node after) {
		Vector3 beforeRight = Vector3.Normalize(Vector3.Cross(before.up, before.forward));
		Vector3 afterRight = Vector3.Normalize(Vector3.Cross(after.up, after.forward));
		Mesh mesh = new Mesh();
		List<Vector3> vertices = new List<Vector3>();
		List<Vector2> uv = new List<Vector2>();
		List<int> triangles = new List<int>();
		vertices.Add(before.position + beforeRight * tunnelRadius);
		vertices.Add(after.position + afterRight * tunnelRadius);
		uv.Add(new Vector2(0, 0));
		uv.Add(new Vector2(1, 0));
		float offsetX, offsetY;
		bool rightSide = false;
		for (float angle = Mathf.PI / 8; angle < Mathf.PI * 2; angle += Mathf.PI / 8) {
			offsetX = Mathf.Cos(angle) * tunnelRadius;
			offsetY = Mathf.Sin(angle) * tunnelRadius;
			vertices.Add(before.position + before.up * offsetY + beforeRight * offsetX);
			vertices.Add(after.position + after.up * offsetY + afterRight * offsetX);
			if (rightSide) {
				uv.Add(new Vector2(0, 0));
				uv.Add(new Vector2(1, 0));
			} else {
				uv.Add(new Vector2(0, 1));
				uv.Add(new Vector2(1, 1));
			}
			rightSide = !rightSide;
			triangles.Add(vertices.Count - 3);
			triangles.Add(vertices.Count - 2);
			triangles.Add(vertices.Count - 4);
			triangles.Add(vertices.Count - 1);
			triangles.Add(vertices.Count - 2);
			triangles.Add(vertices.Count - 3);
		}
		triangles.Add(0);
		triangles.Add(vertices.Count - 2);
		triangles.Add(1);
		triangles.Add(1);
		triangles.Add(vertices.Count - 2);
		triangles.Add(vertices.Count - 1);
		mesh.vertices = vertices.ToArray();
		mesh.uv = uv.ToArray();
		mesh.triangles = triangles.ToArray();
		return mesh;
	}

	private EnemySpawner PickSpawner(int range) {
		List<EnemySpawner> enabledSpawners = new List<EnemySpawner>();
		float totalWeight = 0;
		foreach (EnemySpawner spawner in spawners) {
			if (spawner.minRange <= range && spawner.maxRange >= range) {
				totalWeight += spawner.weight;
				enabledSpawners.Add(spawner);
			}
		}
		float selectedWeight = Random.Range(0, totalWeight);
		foreach (EnemySpawner spawner in enabledSpawners) {
			selectedWeight -= spawner.weight;
			if (selectedWeight <= 0f) {
				return spawner;
			}
		}
		return null;
	}

	private void AddSegment() {
		Node before = rail[rail.Count - 1];

		if (stepsRemaining <= 0) {
			stepsRemaining = (int)Mathf.Floor(Random.Range(5, 15));
			stepDelta = Random.Range(-1, 1);
			stepPhi = 0; // Random.Range(-5, 5);
		}
		--stepsRemaining;

		Quaternion rot = Quaternion.Euler(stepPhi, stepDelta, 0);
		Vector3 dir = rot * before.forward;
		Node after = new Node(before.position + dir * segmentSize, dir, rot * before.up);
		after.mesh = SegmentBetween(before, after);

		if (GameManager.IsRunning()) {
			if (rail.Count > 10 && rail.Count % spawnDensity == 0) {
				Vector2 randomPos = Random.insideUnitCircle * tunnelRadius * 0.3f;
				Vector3 right = Vector3.Cross(after.up, after.forward);
				Vector3 offset = after.up * randomPos.y + right * randomPos.x;

				EnemySpawner spawner = PickSpawner(rail.Count);
				if (spawner != null) {
					GameObject enemy = Object.Instantiate(spawner.prefab, after.position + offset, Quaternion.LookRotation(-after.forward, after.up));
					enemy.GetComponent<Rigidbody>().velocity = Random.insideUnitSphere * 20;
				}
			}
		}

		rail.Add(after);
	}

	private void EnsureDrawDistance() {
		while (rail.Count <= segmentIndex + drawDistance && (GameManager.level == 0 || rail.Count < totalCount)) {
			if (rail.Count > drawDistance + 1) {
				// Free up memory by destroying unneeded meshes
				Object.Destroy(rail[rail.Count - drawDistance - 1].mesh);
				rail[rail.Count - drawDistance - 1].mesh = null;
			}
			this.AddSegment();
		}
	}

	void Start() {
		instance = this;
		this.OnLevelStart();
	}

	void Update() {
		if (GameManager.state != GameState.Dead) {
			segmentPosition += speed * Time.deltaTime;
		}
		while (segmentPosition > 1.0f) {
			segmentPosition -= 1.0f;
			segmentIndex++;
			this.EnsureDrawDistance();
		}
		if (GameManager.level > 0) {
			if (segmentIndex == totalCount - 7) {
				GameManager.PlaySound(levelCompleteSound, 1, 0.05f);
			}
			if (segmentIndex >= totalCount - 1) {
				// End of level
				RenderSettings.fogColor = portalColor;
				bgCamera.backgroundColor = portalColor;
				GameManager.instance.NewLevel();
				return;
			}
		}
		tracker.position = Vector3.Lerp(rail[segmentIndex].position, rail[segmentIndex + 1].position, segmentPosition);
		Vector3 forward = rail[segmentIndex].position + rail[segmentIndex].forward;
		Vector3 next = rail[segmentIndex + 1].position + rail[segmentIndex + 1].forward;
		Vector3 target = Vector3.Lerp(forward, next, segmentPosition);
		tracker.LookAt(target);
		for (int i = segmentIndex; i < Mathf.Min(segmentIndex + drawDistance, totalCount); i++) {
			Graphics.DrawMesh(rail[i].mesh, Matrix4x4.identity, material, 11);
		}
		if (GameManager.level > 0 && rail.Count >= totalCount) {
			float portalIntensity = (segmentIndex - totalCount + drawDistance - 1 + segmentPosition) / drawDistance;
			GameManager.instance.musicSource.volume = 0.4f * (1.0f - portalIntensity);
			portalIntensity *= portalIntensity;
			Color mixColor = Color.Lerp(fogColor, portalColor, portalIntensity);
			RenderSettings.fogColor = mixColor;
			bgCamera.backgroundColor = mixColor;
		}
	}

	public float NearestRail(Vector3 point, out Node bestNode) {
		float bestDist = Mathf.Infinity;
		bestNode = null;
		foreach (Node node in rail) {
			float dist = Vector3.Distance(node.position, point);
			if (dist < bestDist) {
				bestDist = dist;
				bestNode = node;
			} else {
				break;
			}
		}
		return bestDist;
	}

	public override void OnGameStart() {
		ShipBehaviour.killCount = 0;
	}

	public override void OnLevelStart() {
		foreach(string tag in new string[]{ "Enemy", "Bullet", "Powerup" }) {
			foreach(GameObject obj in GameObject.FindGameObjectsWithTag(tag)) {
				Object.Destroy(obj);
			}
		}

		if (GameManager.level < 1) {
			GameManager.PlayMusic(titleMusic);
		} else {
			GameManager.PlayMusic(levelMusic[GameManager.level % levelMusic.Count]);
		}

		int levelNumber = GameManager.level - 1;
		if (levelNumber < 0) {
			levelNumber = 0;
		} else if (levelNumber >= levels.Count) {
			levelNumber = levels.Count - 1;
		}
		LevelDefinition level = levels[levelNumber];
		material = level.material;
		killsPerDrop = level.killsPerDrop;
		spawnDensity = level.spawnDensity;
		spawners = level.spawners;
		totalCount = level.levelLength;

		GameManager.instance.musicSource.volume = 0.4f;
		RenderSettings.fogColor = fogColor;
		bgCamera.backgroundColor = fogColor;

		segmentIndex = 0;
		rail = new List<Node>();
		transform.position = Vector3.zero;
		transform.rotation = Quaternion.identity;
		Node zero = new Node(tracker.position - tracker.forward * segmentSize, tracker.forward, tracker.up);
		Node first = new Node(tracker.position, tracker.forward * segmentSize, tracker.up);
		Node second = new Node(tracker.position + tracker.forward * segmentSize, tracker.forward, tracker.up);
		first.mesh = SegmentBetween(zero, first);
		second.mesh = SegmentBetween(first, second);
		rail.Add(first);
		rail.Add(second);
		tracker.LookAt(second.position);
		this.EnsureDrawDistance();
	}
}
