using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ShipBehaviour))]
[CanEditMultipleObjects]
public class ShipBehaviourEditor : Editor {
	public override void OnInspectorGUI() {
		serializedObject.Update();
		DrawDefaultInspector();
		serializedObject.ApplyModifiedProperties();
		ShipBehaviour shipBehaviour = (ShipBehaviour)target;
		shipBehaviour.gameObject.GetComponent<MeshFilter>().mesh = shipBehaviour.ship.GenerateMesh();
	}
}
