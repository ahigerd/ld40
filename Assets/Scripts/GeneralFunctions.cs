﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralFunctions : MonoBehaviour {

	public static float Approach(float current, float target, float rate, float deadZone = 0.1f)
	{
		if (Mathf.Abs (current - target) < deadZone)
			return target;
		float time = Time.deltaTime;
		float steps = time * 60;
		return (current - target) * Mathf.Pow (0.5f, rate * steps) + target;
	}
}
