using UnityEngine;

public class ShooterBehaviour : MonoBehaviour {
	public GameObject bullet = null;
	public float firingFrequency = 1f;
	public int salvoSize = 1;
	public float fireAngle = 0f;
	public float bulletSpeed = 50f;
	public bool facePlayer = true;

	private float fireCooldown;

	public void Start() {
		fireCooldown = 0.001f;
		GetComponent<Rigidbody>().velocity = Vector3.zero;
	}

	public void Update() {
		GameObject player = GameObject.FindGameObjectsWithTag("PlayerPositioner")[0];
		if (facePlayer) {
			gameObject.transform.LookAt(player.transform);
		}

		if (Vector3.Distance(player.transform.position, gameObject.transform.position) > 300) {
			return;
		}

		fireCooldown -= Time.deltaTime;
		if (fireCooldown < 0) {
			fireCooldown = firingFrequency;
			for (int i = 0; i < salvoSize; i++) {
				GameObject bulletObject = Object.Instantiate(bullet, gameObject.transform.position + gameObject.transform.forward * 2, gameObject.transform.rotation);
				bulletObject.GetComponent<ShipBehaviour>().damageHP /= salvoSize;
				Rigidbody rb = bulletObject.GetComponent<Rigidbody>();
				bulletObject.transform.Rotate(0, 0, 360f / salvoSize * i);
				bulletObject.transform.Rotate(0, fireAngle, 0);
				rb.velocity = bulletObject.transform.forward * bulletSpeed;
			}
		}
	}
}
